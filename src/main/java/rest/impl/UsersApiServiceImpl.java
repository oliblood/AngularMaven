package rest.impl;

import gen.rest.api.UsersApi;
import service.UserService;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class UsersApiServiceImpl implements UsersApi {
    public Response getUsers(SecurityContext securityContext) {
        return Response.ok(UserService.getUsers()).build();
    }
}
