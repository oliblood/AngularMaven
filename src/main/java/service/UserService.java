package service;


import gen.rest.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    public static List<UserDTO> getUsers() {
        List<UserDTO> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserDTO user = new UserDTO();
            user.setId(i);
            user.setName("Teszt Elek " + i);
            users.add(user);
        }
        return users;
    }
}