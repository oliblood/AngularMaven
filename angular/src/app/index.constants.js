/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
    .module('angular')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .value('apiUrl', 'http://localhost:8080/AngularMaven/rest');

})();
