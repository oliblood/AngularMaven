(function () {
  'use strict';

  angular
    .module('angular')
    .factory('MainRS', MainRS);

  /** @ngInject */
  function MainRS($resource, apiUrl) {

    return $resource(apiUrl + '/users', {},
      {
        'get': {
          method: 'get',
          isArray: true
        }
      });

  }
})();
