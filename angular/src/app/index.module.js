(function () {
  'use strict';

  angular
    .module('angular', ['ngResource', 'ngRoute', 'ui.bootstrap', 'toastr']);

})();
